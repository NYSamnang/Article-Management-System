package com.chivoin.spring.entities.filter;

import java.sql.Timestamp;

/**
 * Created by Obi-Voin Kenobi on 06-Jul-17.
 */
public class UserFilter {

    private String username, email, status;
    private Timestamp from_created_date, to_created_date;

    public UserFilter() {
    }

    public UserFilter(String username, String email, String status, Timestamp from_created_date, Timestamp to_created_date) {
        this.username = username;
        this.email = email;
        this.status = status;
        this.from_created_date = from_created_date;
        this.to_created_date = to_created_date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getFrom_created_date() {
        return from_created_date;
    }

    public void setFrom_created_date(Timestamp from_created_date) {
        this.from_created_date = from_created_date;
    }

    public Timestamp getTo_created_date() {
        return to_created_date;
    }

    public void setTo_created_date(Timestamp to_created_date) {
        this.to_created_date = to_created_date;
    }
}
